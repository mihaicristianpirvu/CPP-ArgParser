#include <ArgParser.hpp>

ArgParser::ArgParser(int argc, char **argv) : argc(argc), argv(argv) {}

void ArgParser::parseArguments() {
	std::size_t found;
	std::unordered_set<std::string> uniqueValues;
	int mandatoryCount = this->mandatoryArguments.size();

	if(!argv)
		throw std::runtime_error("No arguments to be parsed");

	for(int i=1; i<argc; i++) {
		std::string arg = std::string(argv[i]), str1, str2;
		found = arg.find_first_of("=");

		if(found == std::string::npos) {
			str1 = arg;
			str2 = "";
		}
		else {
			str1 = arg.substr(0, found);
			str2 = arg.substr(found + 1);
		}

		/* Remove the initial ---- characters */
		auto it = str1.begin();
		while(it != str1.end() && *it == '-')
			++it;

		str1 = str1.substr(std::distance(str1.begin(), it));

		if(uniqueValues.find(str1) != uniqueValues.end())
			throw std::runtime_error("Argument '" + str1 + "' defined more than once.");

		uniqueValues.insert(str1);

		if(str1 == "help") {
			this->helpFlag = true;
			continue;
		}

		auto argument_iterator = this->arguments.find(str1);
		if(argument_iterator == this->arguments.end())
			throw std::runtime_error("Argument '" + str1 + "' not defined.");

		if(this->mandatoryArguments.find(str1) != this->mandatoryArguments.end())
			--mandatoryCount;

		auto argument = argument_iterator->second;
		argument->setValue(str2);
		argument->set();
	}

	/* Print the help, but after parsing all arguments to check for errors. */
	if(this->helpFlag) {
		this->printHelp();
		std::exit(0);
	}

	if(mandatoryCount > 0) {
		this->printUsage();
		throw std::runtime_error("Not all mandatory arguments were defined!");
	}
}

std::string ArgParser::toString() {
	std::string string = "[CPP Arg Parser]";
	return string;
}

bool ArgParser::isDefined(const std::string &arg) const {
	auto argument_it = this->arguments.find(arg);
	if (argument_it == this->arguments.end())
	    return false;	
	return true;
}

bool ArgParser::isSet(const std::string &arg) const {
auto argument_it = this->arguments.find(arg);
	if (argument_it == this->arguments.end())
	    return false;	
	return argument_it->second->isSet();
}

void ArgParser::printHelp() {
	std::cerr<<"[CPP Arg Parser]\n";
	this->printUsage();
}

void ArgParser::setUsage(const std::string &message) {
	this->usageMessage = message;
}

void ArgParser::printUsage() const {
	if(!this->usageMessage.empty())
		std::cerr << this->usageMessage << "\n";

	std::cerr<<"Arguments:\n";
	for(auto &keyValArgument : this->arguments) {
		auto &key = keyValArgument.first;
		auto &value = keyValArgument.second;

		std::cerr << "  " << key << ": " << value->getDescription() << "\n";
	}
}
