/* ValuedArgument.h - Arg Parser - Pirvu Mihai Cristian */
#ifndef VALUED_ARGUMENT_H
#define VALUED_ARGUMENT_H

#include <Argument.hpp>
#include <iostream>
#include <memory>
#include <stdlib.h>
#include <functional>

template <typename T>
struct is_argparsable : std::integral_constant<bool, std::is_same<T, int>::value
	|| std::is_same<T, float>::value
	|| std::is_same<T, double>::value
	|| std::is_same<T, bool>::value
	|| std::is_same<T, std::string>::value> {};

template <typename T=std::string>
class ValuedArgument : public Argument {
public:
	ValuedArgument(const std::string &, const std::string & = "", std::function<bool (T)> = [](T x) { return true; } );
	virtual std::string toString();
	virtual void setValue(std::string &);
	T getValue() const;
	~ValuedArgument() {};

private:
	ValuedArgument() {};
	void checkValue();

	std::function<bool (T)> valueFunction;
	T value;
};

template <typename T>
ValuedArgument<T>::ValuedArgument(const std::string &name, const std::string &description, 
	std::function<bool (T)> valueFunction) : Argument(name, description), valueFunction(valueFunction) {
	static_assert(is_argparsable<T>::value, "Only bool, int, float and double accepted.");
}

template <typename T>
void ValuedArgument<T>::checkValue() {
	if(this->valueFunction(this->value) == false) {
		std::cout<<"Wrong value " << this->value << " for argument '" << this->name << "'\n";
		exit(1);
	}
}

template <typename T>
inline
std::string ValuedArgument<T>::toString() {
	return Argument::toString() + " ; Value: " + std::to_string(this->value);
}

template <>
inline
std::string ValuedArgument<std::string>::toString() {
	return Argument::toString() + " ; Value: " + this->value;
}

template<>
inline
std::string ValuedArgument<bool>::toString() {
	return Argument::toString() + " ; Value: " + (this->value == true ? "true" : "false");
}

template <typename T>
T ValuedArgument<T>::getValue() const {
	return this->value;
}

template <>
inline
void ValuedArgument<std::string>::setValue(std::string &str) {
	if(str == "")
		throw std::runtime_error("Cannot set an empty value to " + this->getName() + "\n");
	this->value = str;
	this->checkValue();
}

template <>
inline
void ValuedArgument<int>::setValue(std::string &str) {
	if(str == "")
		throw std::runtime_error("Cannot set an empty value to " + this->getName() + "\n");
	try {
		this->value = std::stoi(str);
		this->checkValue();
	} catch (const std::exception& e) {
		throw std::runtime_error("Error converting '" + str + "' to integer");
	}
}

template <>
inline
void ValuedArgument<float>::setValue(std::string &str) {
	if(str == "")
		throw std::runtime_error("Cannot set an empty value to " + this->getName() + "\n");
	try {
		this->value = std::stof(str);
		this->checkValue();
	} catch (const std::exception& e) {
		throw std::runtime_error("Error converting '" + str + "' to float");
	}
}

template <>
inline
void ValuedArgument<double>::setValue(std::string &str) {
	if(str == "")
		throw std::runtime_error("Cannot set an empty value to " + this->getName() + "\n");
	try {
		this->value = std::stod(str);
		this->checkValue();
	} catch (const std::exception& e) {
		throw std::runtime_error("Error converting '" + str + "' to double");
	}
}

template <>
inline
void ValuedArgument<bool>::setValue(std::string &str) {
	if(str == "")
		throw std::runtime_error("Cannot set an empty value to " + this->getName() + "\n");
	if(str != "true" && str != "false") {
		throw std::runtime_error("Error converting '" + str + "' to boolean");
	}
	this->value = (str == "true") ? true : false;
	this->checkValue();
}

#endif
