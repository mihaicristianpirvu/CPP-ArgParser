#include <Argument.hpp>

Argument::Argument(const std::string &name, const std::string &description) : name(name), description(description) {}

const std::string &Argument::getName() const {
	return this->name;
}

const std::string &Argument::getDescription() const {
	return this->description;
}

std::string Argument::toString() {
	return "Name: " + this->getName() + " ; Description: " + this->getDescription();
}

void Argument::set(bool value) {
	this->isSetFlag = value;
}

bool Argument::isSet() {
	return this->isSetFlag;
}

void Argument::setValue(std::string &str) {
	throw std::runtime_error("Setting a value to a non-valued argument (" + this->getName() +  ") !");
}