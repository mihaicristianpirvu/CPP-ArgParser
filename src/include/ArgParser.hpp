/* ArgParser.h - Arg Parser - Pirvu Mihai Cristian */
#ifndef ARG_PARSER_HPP_
#define ARG_PARSER_HPP_

#include <assert.h>
#include <algorithm>
#include <iostream>
#include <map>
#include <string>
#include <type_traits>
#include <unordered_set>
#include <vector>
#include <stdexcept>
#include <memory>

#include <Argument.hpp>
#include <ValuedArgument.hpp>

template <typename T> struct is_shared_ptr : std::false_type {};
template <typename T> struct is_shared_ptr<std::shared_ptr<T>> : std::true_type {};

enum ArgType {
	MANDATORY,
	OPTIONAL
};

class ArgParser {
public:

	ArgParser() = delete;
	ArgParser(int, char **);
	void parseArguments();
	template <typename T> void addArgument(T, ArgType = ArgType::OPTIONAL);
	template <typename T> void add(T, ArgType = ArgType::OPTIONAL);
	std::string toString();
	bool isDefined(const std::string &) const;
	bool isSet(const std::string &) const;
	void setUsage(const std::string &);
	void printUsage() const;
	template <typename T> std::shared_ptr<T> getArgument(const std::string &);
	template <typename T> std::shared_ptr<T> get(const std::string &);
	template <typename T> T getValue(const std::string &);
	~ArgParser() {};

private:
	void printHelp();
	template <typename T> void addArgument(T, ArgType, std::true_type);
	template <typename T> void addArgument(T, ArgType, std::false_type);
	
	std::tuple<std::shared_ptr<Argument>, bool> getInternalArgument(const std::string &) const;

	int argc;
	char **argv = NULL;
	bool helpFlag = false;
	std::map<std::string, std::shared_ptr<Argument>> arguments;
	std::unordered_set<std::string> mandatoryArguments;
	std::string usageMessage;
};

template <typename T>
inline
void ArgParser::add(T argument, ArgType argType) {
	this->addArgument(argument, argType);
}

template <typename T>
inline
void ArgParser::addArgument(T argument, ArgType argType) {
	static_assert(std::is_base_of<Argument, typename std::decay<T>::type>::value);
	addArgument(argument, argType, is_shared_ptr<T>{});
}

template <typename T>
void ArgParser::addArgument(T argument, ArgType argType, std::false_type) {
	addArgument(std::make_shared<T>(argument), argType, std::true_type{});
}

template <typename T>
void ArgParser::addArgument(T argument, ArgType argType, std::true_type) {
	if (this->arguments.find(argument->getName()) != this->arguments.end())
		throw std::runtime_error("Argument '" + argument->getName() + "' already defined!");
	this->arguments[argument->getName()] = argument;
	if (argType == ArgType::MANDATORY)
		this->mandatoryArguments.insert(argument->getName());
}

template <typename T>
inline
std::shared_ptr<T> ArgParser::get(const std::string &arg) {
	return this->getArgument<T>(arg);
}

template <typename T>
std::shared_ptr<T> ArgParser::getArgument(const std::string &arg) {
	auto argument_it = this->arguments.find(arg);
	if (argument_it == this->arguments.end())
		throw std::runtime_error("Argument '" + arg + "' not defined!");

	auto argument_ptr = argument_it->second;
	if(!argument_ptr->isSet())
		throw std::runtime_error("Argument '" + arg + "' not set!");

	auto dyn_ptr = std::dynamic_pointer_cast<T>(argument_ptr);
	if(!dyn_ptr)
		throw std::runtime_error("Couldn't convert argument to original type");
	return dyn_ptr;
}

template <typename T>
T ArgParser::getValue(const std::string &arg) {
	auto arg_ptr = this->getArgument<ValuedArgument<T>>(arg);
	return arg_ptr->getValue();
}

#endif
